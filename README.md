# Lumberjack 
Logging gone Plaid

---

Purpose:  Simple logging engine to facilitate logging to files or other destination.

## Installation:

`git clone https://bitbucket.org/ifriz/lumberjack.git`

### Building:

From Visual Studio:
 
Open solution, from Solution Explorer right click on the Lumberjack project, and select Publish.
 
Use the Folder Publish to output a release file to use in another project.

After file publish, use the Lumberjack.dll from the publish folder.


## Usage:

To setup the logger, add the following to your application where the logger will be used:

  `string filePath = @"C:\temp\logs\demo.log";`

  `ILumberjackLogger logger = LumberjackFactory.NewLumberjackLogger(LogType.File, filePath, true);`
  
Besure that the filePath is a location that is writeable.

To write a log entry, invoke the `Log` method as follows:

`logger.Log(LogLevel.DEBUG, "Logger created");`

Use the appropriate logging level as required. 

Log levels are as follows:

- DEBUG
- INFO
- WARN
- ERROR
- PANIC


See the included LumberjackDemo application for example usage.

### Considerations:

Very rudimentary logging, currently only supporting a single log format and logging to filesystem.

In the future, add support for other logging methods, such as to a Database, or to StdOut to facilitate logging for containers.

### Other items:

No explicit method is used to protect sensitive information from being logged.  It is up to the developer to be aware of what is being logged and to where to ensure no unwanted information is logged.

No retention policy for logging currently exists.  As a future roadmap item, the filelogger could be expanded to check log file size and roll over the log if it gets too big.