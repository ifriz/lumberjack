using Lumberjack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace LumberjackTests
{
    [TestClass]
    public class LumberjackFileLoggerTests
    {

        [TestMethod]
        public void TestFileLoggerCreate()
        {
            string filePath = @"C:\logs\test_log_dir_exists.log";

            ILumberjackLogger logger = new FileLogger(filePath);

            Assert.IsTrue(Directory.Exists(Path.GetDirectoryName(filePath)));
        }

        [TestMethod]
        public void TestFileLoggerLog()
        {
            string filePath = @"C:\logs\test_log_file_write.log";

            ILumberjackLogger logger = new FileLogger(filePath);
            logger.Log(LogLevel.DEBUG, "This is a debug message");

            Assert.IsTrue(File.Exists(filePath));

        }

        [TestMethod]
        public void TestLumberjackFileLogSegmentDebug()
        {
            string filePath = @"C:\logs\test_log_file.log";

            ILumberjackLogger logger = new FileLogger(filePath, true);
            logger.Log(LogLevel.DEBUG, "This is a debug message");

            Assert.IsTrue(File.Exists(@"C:\logs\test_log_file_DEBUG.log"));
        }

        [TestMethod]
        public void TestLumberjackFileLogSegmentError()
        {
            string filePath = @"C:\logs\test_log_file.log";

            ILumberjackLogger logger = new FileLogger(filePath, true);
            logger.Log(LogLevel.ERROR, "This is a debug message");

            Assert.IsTrue(File.Exists(@"C:\logs\test_log_file_ERROR.log"));
        }

        [TestMethod]
        public void TestLumberjackFactoryForFile()
        {
            string filePath = @"C:\logs\test_log_file_factory.log";
            ILumberjackLogger logger = LumberjackFactory.NewLumberjackLogger(LogType.File, filePath, true);

            Assert.IsInstanceOfType(logger, typeof(FileLogger));
        }

    }
}