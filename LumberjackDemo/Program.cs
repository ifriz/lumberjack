﻿using Lumberjack;
using System;

namespace LumberjackDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            int n1 = 0, n2 = 1, n3, i, number;
            Console.WriteLine("Lumberjack Logging");

            // Setup logger
            string filePath = @"C:\temp\logs\demo.log";
            ILumberjackLogger logger = LumberjackFactory.NewLumberjackLogger(LogType.File, filePath, true);

            logger.Log(LogLevel.DEBUG, "Logger created");

            // do some work

            Console.Write("Enter the number of elements: ");
            number = int.Parse(Console.ReadLine());
            Console.Write(n1 + " " + n2 + " "); //printing 0 and 1    
            for (i = 2; i < number; ++i) //loop starts from 2 because 0 and 1 are already printed    
            {
                n3 = n1 + n2;
                Console.Write(n3 + " ");
                logger.Log(LogLevel.INFO, $"sequence item {n3}");
                n1 = n2;
                n2 = n3;
            }

            logger.Log(LogLevel.WARN, "This is a warning log messsage");

            Exception ex = new Exception("This is an exception message!");
            logger.Log(LogLevel.ERROR, $"This is an ERROR log message with exception message: {ex}");

            logger.Log(LogLevel.PANIC, "CRITICAL ERROR OCCURRED!!!!");

            Console.WriteLine("Output complete");

        }
    }
}
