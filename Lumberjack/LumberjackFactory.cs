﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lumberjack
{
    public static class LumberjackFactory
    {
        public static ILumberjackLogger NewLumberjackLogger(LogType logType, params object[] args)
        {
            ILumberjackLogger logger;

            switch (logType)
            {
                case LogType.File:
                    // args should match the file logger params
                    logger = new FileLogger(args[0] as string, (bool)args[1]);
                    break;

                default:
                    logger = null;
                    break;
            }

            return logger;
        }
    }
}
