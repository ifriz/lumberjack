﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lumberjack
{
    public interface ILumberjackLogger
    {
        void Log(LogLevel logLevel, string logMessage);
    }
}
