﻿using System;
using System.IO;

namespace Lumberjack
{
    /// <summary>
    /// FileLogger for the Lumberjack logging library/s
    /// </summary>
    public class FileLogger : LumberjackLogger, ILumberjackLogger
    {
 
        protected string logFileDir;
        protected string logFileName;
        protected bool segmentLogsByLevel;

        public FileLogger() : base(LogType.File)
        { }

        public FileLogger(string logPath, bool segmentByLevel = false) : base(LogType.File)
        {
 
            logFileDir = Path.GetDirectoryName(logPath);
            logFileName = Path.GetFileName(logPath);
            segmentLogsByLevel = segmentByLevel;


            // check if logging directory exists.   If not, create it.
            if (!Directory.Exists(logFileDir))
            {
                Directory.CreateDirectory(logFileDir);
            }


        }

        /// <summary>
        /// Write a log entry to a file
        /// </summary>
        /// <param name="logLevel">Severity of the log entry to write</param>
        /// <param name="logMessage">Message to write to log</param>
        public override void Log(LogLevel logLevel, string logMessage)
        {
            string loggingMessage = $"{DateTime.UtcNow.ToLongTimeString()} -- {logLevel.ToString("g")} - {logMessage}";
            string logFilePath;

            if(segmentLogsByLevel)
            {

                logFilePath = Path.Combine(logFileDir, GetFileLevelSuffix(logFileName, logLevel.ToString("g")));

            }
            else
            {
                logFilePath = Path.Combine(logFileDir, logFileName);
            }


            using (StreamWriter file = File.AppendText(logFilePath))
            {
                file.WriteLine(loggingMessage);
                file.Close();
            }
        }

        /// <summary>
        /// Helper method to get the filename with the suffix appended to it.
        /// </summary>
        /// <param name="filename">Filename of log file</param>
        /// <param name="suffix">Suffix to be appended to the file name</param>
        /// <returns>Returns string of filename with suffix added</returns>
        private string GetFileLevelSuffix(string filename, string suffix)
        {
            int idx = filename.LastIndexOf('.');
            string fileNameWithoutExt = Path.GetFileNameWithoutExtension(filename);
            string fileExt = filename.Substring(idx + 1);


            return $"{fileNameWithoutExt}_{suffix}.{fileExt}";
        }


    }
}