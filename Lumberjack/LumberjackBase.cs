﻿namespace Lumberjack
{

    public enum LogType
    {
        File,
        StdOut
    }

    public enum LogLevel
    {
        DEBUG,
        INFO,
        WARN,
        ERROR,
        PANIC
    }

    public abstract class LumberjackLogger
    {
        protected LogType logType;

        protected LumberjackLogger(LogType logType)
        {
            this.logType = logType;
        }

        public abstract void Log(LogLevel logLevel, string logMessage);

    }
}